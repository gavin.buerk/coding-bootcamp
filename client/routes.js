Router.route('welcome', {
    path:"/",
    render:"welcome"
});
Router.route('guess', {
    path: '/guess',
    render: 'guess'
});
Router.route('tictactoe', {
    path: '/tictactoe',
    render: 'tictactoe'
});
Router.route('hangman', {
    path: '/hangman',
    render: 'hangman'
});