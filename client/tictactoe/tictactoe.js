var turn = new ReactiveVar("X");
var message = new ReactiveVar("");

var board = [
    [new ReactiveVar(""),new ReactiveVar(""),new ReactiveVar("")],
    [new ReactiveVar(""),new ReactiveVar(""),new ReactiveVar("")],
    [new ReactiveVar(""),new ReactiveVar(""),new ReactiveVar("")]
];

var gameIsOver = false;

Template.tictactoe.helpers({
    board: function() {
        return board;
    },
    turn: function() {
        return turn.get();
    },
    message: function() {
        return message.get();
    }
});

Template.tictactoe.events({
    'click .square': function(event, template) {
        if (gameIsOver) return;
        var target = $(event.target);
        var col = target.attr('data-col');
        var row = target.closest('.row').attr('data-row');
        handleTurn(row, col);
    }
});

var handleTurn = function(row, col) {
    board[row][col].set(turn.get());
    checkForWin(board, turn.get());
    turn.set(turn.get() === "X" ? "O" : "X");
};

var checkForWin = function (board, turn) {
    checkRows(board, turn);
    checkCols(board, turn);
    checkDiags(board, turn);
};

var checkRows = function (board, turn) {
    for (var col = 0; col < 3; col++) {
        var count = 0;
        for (var row = 0; row < 3; row++) {
            if (board[row][col].get() === turn) count++;
        }
        if (count === 3) {
            gameOver(turn);
        }
    }
};

var checkCols = function (board, turn) {
    for (var row = 0; row < 3; row++) {
        var count = 0;
        for (var col = 0; col < 3; col++) {
            if (board[row][col].get() === turn) count++;
        }
        if (count === 3) {
            gameOver(turn);
        }
    }
};

var checkDiags = function (board, turn) {
    var count = 0;
    if (board[0][0].get() === turn) count++;
    if (board[1][1].get() === turn) count++;
    if (board[2][2].get() === turn) count++;
    if (count === 3) {
        gameOver(turn);
    }


    count = 0;
    if (board[2][0].get() === turn) count++;
    if (board[1][1].get() === turn) count++;
    if (board[0][2].get() === turn) count++;
    if (count === 3) {
        gameOver(turn);
    }
};

var gameOver = function (turn) {
    gameIsOver = true;
    message.set('Congratulations!  ' + turn + ' Wins!');
    animate($('.messageAnimation'), "bounceIn");
};



//// This is an alternate implementation of checkForWin that is simpler computationally, but more difficult to understand
//var checkForWin = function(board, turn, row, col) {
//    var rowCount = 0, colCount = 0, diagCount = 0, reverseDiagCount = 0;
//    for (var i = 0; i < 3; i++) {
//        if (board[row][i].get() === turn) {
//            colCount++;
//        }
//        if (board[i][col].get() === turn) {
//            rowCount++;
//        }
//        if (board[i][i].get() === turn) {
//            diagCount++;
//        }
//        if (board[i][2-i].get() === turn) {
//            reverseDiagCount++;
//        }
//        if (rowCount === 3 || colCount === 3 || diagCount === 3 || reverseDiagCount === 3) {
//            gameOver(turn);;
//        }
//    }
//};