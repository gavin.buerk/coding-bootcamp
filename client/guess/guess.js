var target = Math.floor((Math.random() * 100) + 1);
var guessCount = 0;
var message = new ReactiveVar('');
var gameOver = new ReactiveVar(false);

Template.guess.helpers({
    message: function() {
        return message.get();
    },
    gameIsOver: function() {
        return gameOver.get();
    }
});

Template.guess.events({
    'submit form': function(event, template) {
        event.preventDefault();
        if (gameOver.get()) return;
        var $guessInput = $('#guessInput');
        var guess = Number($guessInput.val());
        $guessInput.val('');
        handleGuess(guess);
        animate($('.animatedMessage'), 'bounceIn');

    },
    'click #playAgainButton':function(event, template) {
        handlePlayAgain();
    }
});

var handleGuess = function(guess) {
    guessCount++;
    console.log("Target is " + target + ".  guess is " + guess);
    if (guess === target) {
        message.set("Correct!  You win!.  You took " + guessCount + " chances");
        gameOver.set(true);
    } else if (guess > target) {
        message.set("Too high, try lower");
    } else {
        message.set("Too low, try higher");
    }
};

var handlePlayAgain = function () {
    target = Math.floor((Math.random() * 100) + 1);
    guessCount = 0;
    message.set("");
    gameOver.set(false);
    $('#guessInput').focus();
};