var board = new ReactiveVar("");
var gameIsOver = new ReactiveVar(false);
var guessedLetters = new ReactiveVar([]);
var guessesRemaining = new ReactiveVar(5);
var win = new ReactiveVar();
var puzzles = [
    "HANGMAN",
    "WELCOME TO FUSE",
    "PROGRAMMING IS FUN",
    "LEARNING ROCKS",
    "METEOR IS VERY COOL",
];
var puzzle = puzzles[Math.floor((Math.random() * puzzles.length))];

Template.hangman.helpers({
    board: function() {
        return board.get();
    },
    showReset:function() {
        return gameIsOver.get()
    },
    guessedLetters: function() {
        return guessedLetters.get();
    },
    guessesRemaining: function() {
        return guessesRemaining.get();
    },
    hangmanSprite: function() {
        if (guessesRemaining.get() === 5) return '';
        if (guessesRemaining.get() === 4) return 'pic1';
        if (guessesRemaining.get() === 3) return 'pic2';
        if (guessesRemaining.get() === 2) return 'pic3';
        if (guessesRemaining.get() === 1) return 'pic4';
        if (guessesRemaining.get() === 0) return 'pic5';
    },
    winOrLose: function() {
        return win.get() ? "WIN!" : "lose";
    }
});

Template.hangman.events({
    'submit #guessForm': function(event, template) {
        event.preventDefault();
        var $guessLetterInput = $('#guessLetterInput');
        var guessedLetter = $guessLetterInput.val().toUpperCase();
        handleGuessedLetter(guessedLetter);
        $guessLetterInput.val('');
        $guessLetterInput.focus();
    },
    'click #reset': function(event, template) {
        resetGame();
    },
    'click #back': function(event, template) {
        resetGame();
        Router.go('welcome');
    },
    'click #create': function(event, template) {
        openModal();
    },
    'click .closeModal' :function(event, template) {
        closeModal();
    },
    'keyup':function(event, template) {
        if (event.keyCode === 27 /*escape*/ && modalIsVisible()) {
            closeModal();
        }
    },
    'submit #createForm':function(event, template) {
        event.preventDefault();
        resetGame();
        puzzle = $('#puzzleInput').val().toUpperCase();
        compareBoard();
        closeModal();
    }
});

var modalIsVisible = function () {
    return $('.modal').css('display') === "block";
};

var openModal = function() {
    var $puzzleInput = $('#puzzleInput');
    $puzzleInput.val('');
    $('.modal').css('display', 'block');
    animate($('.modalContent'), 'slideInDown', function() {
        $puzzleInput.focus();
    });
};

var closeModal = function() {
    $('.modal').css('display', 'none');
    $('#guessLetterInput').focus();
};

var handleGuessedLetter = function (guessedLetter) {
    if (!_.contains(guessedLetters.get(), guessedLetter)) {
        var gl = guessedLetters.get();
        gl.push(guessedLetter);
        guessedLetters.set(gl);
    }
    if (!_.contains(puzzle, guessedLetter)){
        guessesRemaining.set(guessesRemaining.get() - 1);
        animate($('.animatedSprite'), 'bounceIn');
        if (guessesRemaining.get() === 0) {
            gameIsOver.set(true);
            win.set(false);
        }
    }

    compareBoard();
};

var resetGame = function() {
    gameIsOver.set(false);
    guessedLetters.set([]);
    guessesRemaining.set(5);
    puzzle = puzzles[Math.floor((Math.random() * puzzles.length))];
    compareBoard();
    $('#guessLetterInput').focus();
};

var compareBoard = function() {
    var tmpBoard = "";
    var foundCount = 0;
    _.each(puzzle, function(letter) {
        if (_.contains(guessedLetters.get(), letter) || _.contains(".,! ?':;", letter)) {
            tmpBoard += letter;
            foundCount++;
        } else {
            tmpBoard += "_";
        }
    });
    if (foundCount === puzzle.length) {
        gameIsOver.set(true);
        win.set(true);
    }
    board.set(tmpBoard);
};
compareBoard();
