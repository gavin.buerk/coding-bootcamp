animate = function($element, animation, callback) {
    $element.removeClass('animated ' + animation);
    $element.addClass('animated ' + animation);
    $element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $element.removeClass('animated ' + animation);
        if (callback) callback();
    });
};